<?php
session_start();
if(isset($_SESSION['login']))$login=$_SESSION['login'];
if(isset($_SESSION['pass']))$password=$_SESSION['pass'];
if(isset($_SESSION['passRepeat']))$password2=$_SESSION['passRepeat'];
if(isset($_SESSION['sex']))$sex=$_SESSION['sex'];
if(isset($_SESSION['cities']))$city=$_SESSION['cities'];
if(isset($_SESSION['games']))$games=$_SESSION['games'];
if(isset($_SESSION['aboutMe']))$about=$_SESSION['aboutMe'];
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Лабораторна робота 11</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="lang">
    <a href="index.php?lang=Англійська"><img src="images/en.JPG" alt="ENG"></a>
    <a href="index.php?lang=Українська"><img src="images/ukr.png" alt="UKR"></a>
    <a href="index.php?lang=Російська"><img src="images/ru.png" alt="RU"></a>
    <a href="index.php?lang=Французька"><img src="images/fr.jpg" alt="FR"></a>
    <a href="index.php?lang=Італійська"><img src="images/it.jpg" alt="IT"></a>
</div>
<h1>Лабораторна робота 11</h1>
<form method="post" action="index.php"  enctype="multipart/form-data">
    <div>
        <label for="log">Логін:</label>
        <input type="text" id="log" name="login" required value="<?php echo $login;?>">
    </div>
    <div>
        <label for="pas">Пароль:</label>
        <input type="password" id="pas" name="pass" required value="<?php echo $password;?>">
    </div>
    <div>
        <label for="pasR">Пароль(ще раз):</label>
        <input type="password" id="pasR" name="passRepeat" required value="<?php echo $password2;?>">
    </div>
    <div>
        <label>Стать:</label>
        <label><input type="radio" name="sex" value="чоловік"<?php if($sex=="чоловік")echo 'checked';?>/> чоловік</label>
        <label><input type="radio" name="sex" value="жінка" <?php if($sex=="жінка")echo 'checked';?>/> жінка</label>
    </div>
    <div>
        <label for="city">Місто:</label>
        <select name="cities" id="city">
            <option value="Житомир" <?php if($city=="Житомир")echo 'selected';?>>Житомир</option>
            <option value="Львів" <?php if($city=="Львів")echo 'selected';?>>Львів</option>
            <option value="Одеса" <?php if($city=="Одеса")echo 'selected';?>>Одеса</option>
            <option value="Київ" <?php if($city=="Київ")echo 'selected';?>>Київ</option>
        </select>
    </div>
    <div>
        <label>Улюбленні ігри:</label>
        <label><input type="checkbox" name="games[]" value="футбол"<?php if(isset($games)
                && array_search("футбол",$games)!==false)echo 'checked';?>>
            футбол</label>
        <label><input type="checkbox" name="games[]" value="баскетбол" <?php if(isset($games)
                && array_search("баскетбол",$games)!==false)echo 'checked';?>>
            баскетбол</label>
        <label><input type="checkbox" name="games[]" value="волейбол" <?php if(isset($games)
                && array_search("волейбол",$games)!==false)echo 'checked';?>>
            волейбол</label>
        <label><input type="checkbox" name="games[]" value="шахи" <?php if(isset($games)
            && array_search("шахи",$games)!==false) echo 'checked';?>>
            шахи</label>
        <label><input type="checkbox" name="games[]" value="WoT" <?php if(isset($games)
                && array_search("WoT",$games)!==false)echo 'checked';?>>
            World of Tanks</label>
    </div>
    <div>
        <label for="about">Про себе:</label>
        <textarea id="about" name="aboutMe"><?php echo $about;?></textarea>
    </div>
    <div>
        <label>Фотографія:</label>
        <input type="file" name="photo">
    </div>
    <input type="submit" value="Зареєструватися">
</form>
</body>
</html>