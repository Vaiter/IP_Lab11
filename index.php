<?php
if(isset($_GET['lang'])){
    $lang=$_GET['lang'];
    setcookie('lang',$lang,time()+(3600*24*183));
}
else if(isset($_COOKIE['lang'])){
    $lang=$_COOKIE['lang'];
}
session_start();
if(isset($_POST["login"])){
    $login=$_POST["login"];
    $_SESSION["login"]=$login;
}
if(isset($_POST["pass"]) && isset($_POST["passRepeat"]) ) {
    $password = $_POST["pass"];
    $_SESSION["pass"]=$password;
    $password2 = $_POST["passRepeat"];
    $_SESSION["passRepeat"]=$password2;
    $passCompare = '';
    if ($password === $password2) {
        $passCompare = "співпадає";
    } else {
        $passCompare = "не співпадає( перший " . strlen($password) . " символів, другий " . strlen($password2) . " символів)";
    }
}
if(isset($_POST['sex'])){
    $sex=$_POST["sex"];
    $_SESSION["sex"]=$sex;
}
if(isset($_POST['cities'])){
    $city=$_POST["cities"];
    $_SESSION["cities"]=$city;
}
if(isset($_POST['games'])) {
    $games = array();
    foreach ($_POST['games'] as $game) {
        array_push($games, $game);
    }
    $_SESSION["games"]=$games;
}
if(isset($_POST['aboutMe'])){
    $_SESSION["aboutMe"]=$_POST['aboutMe'];
    $about=nl2br($_POST["aboutMe"]);
}
if(isset($_FILES['photo'])) {
    $uploaddir = 'images/';
    move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir .
        $_FILES['photo']['name']);
    $path = "\"" . $uploaddir . $_FILES['photo']['name'] . "\"";
}
?>
<html>
<head>
    <title>Результати обробки форми</title>
    <link rel="stylesheet" href="style2.css">
</head>
<body>
<h1>Лабораторна робота 11</h1>
<h2>Результати обробки форми</h2>
<div class="info">
    <div>
        <label>Логін: </label><span><?php echo $login?></span>
    </div>
    <div>
        <label>Пароль: </label> <span><?php echo $passCompare?></span>
    </div>
    <div>
        <label>Місто: </label> <span><?php echo $city?></span>
    </div>
    <div>
        <label>Стать: </label> <span><?php echo $sex?></span>
    </div>
    <div>
        <label>Улюблені ігри: </label>
        <ul>
            <?php if(isset($games)){
                foreach ($games as $game){
                    echo "<li>".$game."</li>";
                }
            } ?>
        </ul>
    </div>
    <div>
        <label>Про себе: </label> <span><?php echo $about?></span>
    </div>
    <div>
        <label>Фотографія: </label> <img src=<?php echo $path?>>
    </div>
    <a href="main.php">Повернутися на головну сторінку</a>
</div>
<div class="lang">
    <?php
        echo "<span>Вибрана мова: </span>$lang";
        ?>
</div>
</body>
</html>
